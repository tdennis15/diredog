# DireDog
## Milestone 4: Team Project
### Identification of Risks
1. Schedule Risks:
    * **Scope Creep**: Project becoming too big, can lead to losing the original idea in the weeds
        * Each sprint must be crafted to center around features in a strategic way, so that sections do not go unfinished or work unutilized.
    * **Complex functionality**: Failure to to identify complexity, leading to team members spending excessive or unpredicted amounts of time on tasks
        * Effort points must be assigned carefully, and team members open to request help 
    * **Incorrect Time Estimation**: Tasks may be assigned an inaccurate number of effort points due to unforeseen skills or time requirements
        * Each task should be thought through carefully during the backlog grooming, and sprint planning, and any potential concerns brought up as early as possible to be resolved without loss of work time.
        
2. Usability Risks:
    * **Counterintuitive User Interface:** An unusable user interface will prevent adoption from significant part of target users (those who are middleaged and above, or not 'tech-savvy').
        * The design of the web application (not just front end, but how all the moving parts interact with each other) must be thought out early in order for the user experience to have the structure it needs to function well. 