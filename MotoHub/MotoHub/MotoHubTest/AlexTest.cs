﻿using NUnit.Framework;
using MotoHub.Controllers;
using System;

namespace MotoHubTest
{
    [TestFixture]
    public class TestClubCounts
    {
        [Test]
        public void Clubs_OnlyOneClubMember_ReturnsTrueIfClubMemberCountIsOne()
        {
            var controller = new ClubsController();
            var result = controller.OnlyOneClubMember(1);
            Assert.IsTrue(result);
        }

        [Test]
        public void Clubs_OnlyOneClubMember_ReturnsFalseIfClubMemberCountIsMoreThanOne()
        {
            var controller = new ClubsController();
            var result = controller.OnlyOneClubMember(5);
            Assert.IsFalse(result);
        }

        [Test]
        public void Clubs_OnlyOneClubMember_ThrowsExceptionIfParameterIsLessThanOne()
        {
            try
            {
                var controller = new ClubsController();
                var result = controller.OnlyOneClubMember(-1);
            }
            catch(Exception)
            {
                Assert.Pass();
            }
            Assert.Fail("Expected exception was not thrown.");
        }

    }
}


/*public void ThenReturnTheFaqViewModel()
{
    // Arrange
    var faqs = new List<Faq>
    {
        new Faq {Id = "1", Answer = "Home", Question = "Where Do you Live?"},
        new Faq {Id = "2", Answer = "Since I was 11", Question = "When did you start programming?"},
        new Faq {Id = "3", Answer = "In Pennsylvania", Question = "Where were you born?"}
    };
    var faqRepository = new Mock<IFaqRepository>();
    faqRepository.Setup(e => e.GetAll()).Returns(faqs.AsQueryable());
    var controller = new FaqController(faqRepository.Object);
    // Act 
    var result = controller.Index() as ViewResult;
    var model = result.Model as FaqViewModel;
    // Assert
    Assert.IsNotNull(result);
    Assert.AreEqual(3, model.FAQs.Count());
}
*/