﻿using MotoHub.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MotoHub.DAL;

namespace MotoHub.MotoHubDevLibrary
{

    public class DevTools
    {
        /// <summary>
        /// Gets the MotoHubUser Entity that is related to the AspNetIdentityID string input.
        /// </summary>
        /// <param name="id">The AspNetIdentityID search criteria</param>
        /// <param name="db">The current database context that the method should be operating in.</param>
        /// <returns>MotoHubUser that has a matching ID to the string input.</returns>
        [Authorize]
        public MotoHubUser GetCurrentUser(MotoHubDbContext db, string id)
        {
            MotoHubUser queriedUser = db.MotoHubUsers.Where(m => m.AspNetIdentityID == id).FirstOrDefault();

            return queriedUser;
        }

        /// <summary>
        /// Gets the MotoHubUser Entity that is related to the AspNetIdentityID string input.
        /// </summary>
        /// <param name="id">The AspNetIdentityID search criteria</param>
        /// <param name="db">The current database context that the method should be operating in.</param>
        /// <returns>MotoHubUser that has a matching ID to the string input.</returns>
        [Authorize]
        public MotoHubUser GetCurrentUser(IMotoHubDbContext db, string id)
        {
            MotoHubUser queriedUser = db.MotoHubUsers.Where(m => m.AspNetIdentityID == id).FirstOrDefault();

            return queriedUser;
        }

        /// <summary>
        /// Gets the UserProfile of a MotoHubUser.
        /// </summary>
        /// <param name="hubUser">The MotoHub user for the Profile to be returned.</param>
        /// <param name="db">The current database context that the method should be operating in.</param>
        /// <returns>UserProfile Entity</returns>
        [Authorize]
        public UserProfile GetCurrentUserProfileModel(MotoHubDbContext db, MotoHubUser hubUser)
        {
            UserProfile queriedUser = db.UserProfiles.Where(m => m.MotoHubUserID == hubUser.MotoHubUserID).FirstOrDefault();

            return queriedUser;
        }

        /// <summary>
        /// Gets the UserProfile of a MotoHubUser.
        /// </summary>
        /// <param name="hubUser">The MotoHub user for the Profile to be returned.</param>
        /// <param name="db">The current database context that the method should be operating in.</param>
        /// <returns>UserProfile Entity</returns>
        [Authorize]
        public UserProfile GetCurrentUserProfileModel(IMotoHubDbContext db, MotoHubUser hubUser)
        {
            UserProfile queriedUser = db.UserProfiles.Where(m => m.MotoHubUserID == hubUser.MotoHubUserID).FirstOrDefault();

            return queriedUser;
        }
    }
}