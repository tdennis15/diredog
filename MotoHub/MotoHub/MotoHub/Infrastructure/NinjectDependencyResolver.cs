﻿using MotoHub.Repositories;
using MotoHub.Repositories.RepositoryInterfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MotoHub.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            this.kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            kernel.Bind<IEventRepository>().To<EventRepository>();
            kernel.Bind<ICarClassRepository>().To<CarClassRepository>();
            kernel.Bind<IClubRepository>().To<ClubRepository>();
            kernel.Bind<IEventResultsRepository>().To<EventResultsRepository>();
            kernel.Bind<IMembershipRepository>().To<MembershipRepository>();
            kernel.Bind<IMotoHubUserRepository>().To<MotoHubUserRepository>();
            kernel.Bind<IRoleRepository>().To<RoleRepository>();
            kernel.Bind<IRunDataRepository>().To<RunDataRepository>();
            kernel.Bind<IRunGroupRepository>().To<RunGroupRepository>();
            kernel.Bind<IRunSectionRepository>().To<RunSectionRepository>();
            kernel.Bind<IRegistrationRepository>().To<RegistrationRepository>();
            kernel.Bind<IGalleriesRepository>().To<GalleriesRepository>();
            kernel.Bind<IPhotoAlbumsRepository>().To<PhotoAlbumsRepository>();
            kernel.Bind<IPhotosRepository>().To<PhotosRepository>();
            kernel.Bind<IGarageRepository>().To<GarageRepository>();

        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
    }
}