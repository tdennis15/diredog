CREATE TABLE [dbo].[MotoHubUsers] (
	
	[MotoHubUserID]		INT IDENTITY (1,1)	NOT NULL,
	[Username]			NVARCHAR(64)		NULL,
	[LoginCount]		INT					DEFAULT 1,
	[UserQRCode]		NVARCHAR(128),
	[AspNetIdentityID]	NVARCHAR(128)		NOT NULL,
	[GarageID]			INT					NULL,

	CONSTRAINT [PK_dbo.MotoHubUsers] PRIMARY KEY CLUSTERED ([MotoHubUserID] ASC)
);

CREATE TABLE [dbo].[Gallery] (
	[GalleryID]			INT				IDENTITY(1,1)	NOT NULL

	CONSTRAINT [PK_dbo.Gallery] PRIMARY KEY CLUSTERED ([GalleryID] ASC)
);

CREATE TABLE [dbo].[PhotoAlbums] (
	[PhotoAlbumID]		INT				IDENTITY (1,1)	NOT NULL,
	[GalleryID]			INT				NOT NULL,
	[PhotoAlbumName]	NVARCHAR(64)	NULL

	CONSTRAINT [PK_dbo.PhotoAlbums] PRIMARY KEY CLUSTERED ([PhotoAlbumID] ASC),

	CONSTRAINT [FK_dbo.PhotoAlbums.GalleryID]
	FOREIGN KEY ([GalleryID]) REFERENCES [dbo].[Gallery] ([GalleryID])

);

CREATE TABLE [dbo].[Photos] (
	[PhotoID]		INT				IDENTITY (1,1)	NOT NULL,
	[PhotoAlbumID]	INT				NOT NULL,
	[PhotoName]		NVARCHAR(32)	NOT NULL,
	[ContentType]	NVARCHAR(64)	NOT NULL,
	[PhotoData]		IMAGE			NOT NULL,
	[PhotoCaption]	NVARCHAR(128)	NULL

	CONSTRAINT [PK_dbo.Photos] PRIMARY KEY CLUSTERED ([PhotoID] ASC)

	CONSTRAINT [FK_dbo.Photos.PhotoAlbumID] 
	FOREIGN KEY ([PhotoAlbumID]) REFERENCES [dbo].[PhotoAlbums] ([PhotoAlbumID]),

	
);

CREATE TABLE [dbo].[MotoHubRoles] (
	
	[RoleID]		INT	IDENTITY (1,1)	NOT NULL,
	[RoleTitle]		NVARCHAR (32)		NOT NULL,
	
	CONSTRAINT [PK_dbo.MotoHubRoles] PRIMARY KEY CLUSTERED ([RoleID] ASC)
	
);

CREATE TABLE [dbo].[UserProfile] (
	[MotoHubUserID]				INT					NOT NULL,
	[FirstName]					NVARCHAR(64)		NOT NULL,
	[LastName]					NVARCHAR(64)		NOT NULL,
	[DateOfBirth]				DATE				NULL,
	[UserCity]					NVARCHAR(64)		NULL,
	[Bio]						NVARCHAR (max)		NULL,
	[ProfileIsPrivate]			Bit					NOT NULL,
	[GalleryID]					INT					NOT NULL,
	[ProfilePhotoID]			INT					NULL,
	
	CONSTRAINT [PK_dbo.UserProfile.MotoHubUserID] PRIMARY KEY CLUSTERED	([MotoHubUserID] ASC),

	CONSTRAINT [FK_dbo.UserProfile.MotoHubUserID] 
	FOREIGN KEY ([MotoHubUserID]) REFERENCES [dbo].[MotoHubUsers] ([MotoHubUserID]) 
	ON DELETE CASCADE ON UPDATE CASCADE,

	CONSTRAINT [FK_dbo.UserProfile.Photos] 
	FOREIGN KEY ([ProfilePhotoID]) REFERENCES [dbo].[Photos] ([PhotoID])
	ON DELETE CASCADE ON UPDATE CASCADE,

	CONSTRAINT [FK_dbo.UserProfile.GalleryID]
	FOREIGN KEY ([GalleryID]) REFERENCES [dbo].[Gallery] ([GalleryID])

);


CREATE TABLE [dbo].[Clubs] (

	[ClubsID]			INT IDENTITY (1,1)		NOT NULL,
	[ClubName]			NVARCHAR (50)			NOT NULL,
	[ClubSize]			INT						Default 1,
	[ClubIsActive]		Bit						NOT NULL,
	[ClubDescription]	NVARCHAR(max)			NULL,
	[ClubLocationStreet]		NVARCHAR(64),
	[ClubLocationCity]			NVARCHAR(64),
	[ClubLocationState]			NVARCHAR(64),
	[ClubLocationZip]			INT,
	[ProfilePhotoID]			INT				NULL,
	[GalleryID]					INT				NULL,

	CONSTRAINT [PK_dbo.Clubs] PRIMARY KEY CLUSTERED ([ClubsID] ASC),

	CONSTRAINT [PK_dbo.Clubs.ProfilePhotoID]
	FOREIGN KEY ([ProfilePhotoID]) REFERENCES [dbo].[Photos] ([PhotoID])
	ON DELETE CASCADE ON UPDATE CASCADE,
  
	CONSTRAINT [FK_dbo.Clubs.GalleryID]
	FOREIGN KEY ([GalleryID]) REFERENCES [dbo].[Gallery] ([GalleryID])
);

CREATE TABLE [dbo].[Committee] (

	[CommitteeID]		INT	IDENTITY(1,1)	NOT NULL,
	[Position]			NVARCHAR(64)		NOT NULL,
	[CommitteStatus]	INT,
	[StartDate]			Date,
	[EndDate]			Date,

	CONSTRAINT [PK_dbo.Committee] PRIMARY KEY CLUSTERED (CommitteeID ASC)

);

CREATE TABLE [dbo].[Membership] (
	
	[MotoHubUserID]		INT					NOT NULL,
	[ClubID]			INT					NOT NULL,
	[RoleID]			INT					NOT NULL,
	[CommitteeID]		INT					NULL,
	[Status]			Bit					NOT NULL,
	[DuesPaid]			Bit					NOT NULL

	CONSTRAINT [PK_dbo.Membership] PRIMARY KEY CLUSTERED ([MotoHubUserID] ASC, [ClubID] ASC),

	CONSTRAINT [FK_dbo.Membership_dbo.MotoHubUserID] 
	FOREIGN KEY ([MotoHubUserID]) REFERENCES [dbo].[MotoHubUsers] ([MotoHubUserID]) 
	ON DELETE CASCADE ON UPDATE CASCADE,

	CONSTRAINT [FK_dbo.Membership_dbo.ClubID] 
	FOREIGN KEY ([ClubID]) REFERENCES [dbo].[Clubs] ([ClubsID]) 
	ON DELETE CASCADE ON UPDATE CASCADE,

	CONSTRAINT [FK_dbo.Membership_dbo.MotoHubRoles]
	FOREIGN KEY ([RoleID]) REFERENCES [dbo].[MotoHubRoles] ([RoleID])
	ON DELETE CASCADE ON UPDATE CASCADE,

	CONSTRAINT [FK_dbo.Membership.dbo.Committee]
	FOREIGN KEY ([CommitteeID]) REFERENCES [dbo].[Committee] ([CommitteeID])
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[Events] (
	
	[EventsID]			INT	IDENTITY (1,1)		NOT NULL,
	[MotoHubUserID]		INT						NOT NULL,
	[ClubID]			INT						NOT NULL,
	[EventName]			NVARCHAR(64)			NOT NULL,
	[EventDate]			DATE					NOT NULL,
	[EventLocationStreet]		NVARCHAR(64)	NOT NULL,
	[EventLocationCity]			NVARCHAR(64)	NOT NULL,
	[EventLocationState]		NVARCHAR(64)	NOT NULL,
	[EventLocationZip]			INT				NOT NULL,
	[ProfilePhotoID]			INT				NULL,
	[GalleryID]					INT				NOT NULL,

	CONSTRAINT [PK_dbo.Events] PRIMARY KEY CLUSTERED ([EventsID] ASC),

	CONSTRAINT [FK_dbo.Events.ClubID] 
	FOREIGN KEY ([ClubID]) REFERENCES [dbo].[Clubs] ([ClubsID])
	ON DELETE CASCADE ON UPDATE CASCADE,

	CONSTRAINT [FK_dbo.Events.MotoHubUserID]
	FOREIGN KEY ([MotoHubUserID]) REFERENCES [dbo].[MotoHubUsers] ([MotoHubUserID])
	ON DELETE CASCADE ON UPDATE CASCADE,

	CONSTRAINT [FK_dbo.Events.ProfilePhotoID.ProfilePhotoID]
	FOREIGN KEY ([ProfilePhotoID]) REFERENCES [dbo].[Photos] ([PhotoID]),

	CONSTRAINT [FK_dbo.Events.GalleryID]
	FOREIGN KEY ([GalleryID]) REFERENCES [dbo].[Gallery] ([GalleryID])
);
 
CREATE TABLE [dbo].[Garage] (
	[GarageID]			INT IDENTITY (1,1)	NOT NULL,
	[MotoHubUserID]		INT					NOT NULL,
	

	CONSTRAINT [PK_dbo.Garage] PRIMARY KEY CLUSTERED ([GarageID] ASC),

	CONSTRAINT [FK_dbo.Garage.MotoHubUserID]
	FOREIGN KEY ([MotoHubUserID]) REFERENCES [dbo].[MotoHubUsers] ([MotoHubUserID]) 
	ON DELETE CASCADE ON UPDATE CASCADE,
		
);

CREATE TABLE [dbo].[CarMakes] (
	[CarMakeID]		INT	IDENTITY (1,1)	     NOT NULL,
	[Make]			NVARCHAR(64) NOT NULL,

	Constraint [PK_dbo.CarMake] Primary Key Clustered ([CarMakeID] ASC),
	);

CREATE TABLE [dbo].[CarColor](
	[CarColorID]	INT	 IDENTITY (1,1)        NOT NULL,
	[Color]			NVARCHAR(64) NOT NULL,

	Constraint [PK_dbo.CarColor] Primary Key Clustered ([CarColorID] ASC),
);

CREATE TABLE [dbo].[CarClass](
	[CarClassID]	INT	IDENTITY (1,1)	     NOT NULL,
	[Class]			NVARCHAR(64) NOT NULL,

	Constraint [PK_dbo.CarClass] Primary Key Clustered ([CarClassID] ASC),
);

CREATE TABLE [dbo].[CarType](
	[CarTypeID]			INT	IDENTITY (1,1)	     NOT NULL,
	[CarType]			NVARCHAR(64) NOT NULL,

	Constraint [PK_dbo.CarType] Primary Key Clustered ([CarTypeID] ASC),
);

CREATE TABLE [dbo].[Car] (
	[CarID]		        INT	IDENTITY(1,1)	NOT NULL,
	[MotoHubUserID]		INT		     		NOT NULL,
    [GuestID]           INT,
    [GarageID]			INT			     	NOT NULL,

	[CarMakeID]			INT				    NOT NULL,
	[CarModel]			NVARCHAR(64)	    NOT NULL,
	[CarYear]			INT				    NOT NULL,
	[CarColorID]		INT,
    [CarTypeID]			INT,
	[CarClassID]		INT,
	[CarQRCode]			NVARCHAR(128),
    
	Constraint [PK_dbo.Car] Primary Key Clustered ([CarID] ASC),
    
    CONSTRAINT [FK_dbo.Car.MotoHubUserID] 
    FOREIGN KEY ([MotoHubUserID]) REFERENCES [dbo].[MotoHubUsers] ([MotoHubUserID])
    ON DELETE CASCADE ON UPDATE CASCADE,

	CONSTRAINT [FK_dbo.Car.GarageID] 
    FOREIGN KEY ([GarageID]) REFERENCES [dbo].Garage ([GarageID]),

	CONSTRAINT [FK_dbo.Car.MakeID] 
    FOREIGN KEY (CarMakeID) REFERENCES [dbo].CarMakes ([CarMakeID]),

	CONSTRAINT [FK_dbo.Car.ColorID] 
    FOREIGN KEY ([CarColorID]) REFERENCES [dbo].CarColor ([CarColorID]),

	CONSTRAINT [FK_dbo.Car.CarTypeID] 
    FOREIGN KEY ([CarTypeID]) REFERENCES [dbo].CarType ([CarTypeID]),

	CONSTRAINT [FK_dbo.Car.CarClassID] 
    FOREIGN KEY ([CarClassID]) REFERENCES [dbo].CarClass ([CarClassID])
	
);

CREATE TABLE [dbo].[Registration] (
	[RegistrationID]	INT		IDENTITY (1,1) NOT NULL,
	[UserID]			INT		NOT NULL,
	[EventID]			INT		NOT NULL,
	[CarID]				INT		NULL,
	[FeesPaid]			INT		NULL,
	[ClubsID]			INT		NULL, 

	CONSTRAINT [PK_dbo.Registration] PRIMARY KEY CLUSTERED ([RegistrationID] ASC),

	CONSTRAINT [FK_dbo.Registration.MotoHubUsers]
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[MotoHubUsers] ([MotoHubUserID]),
	
	CONSTRAINT [FK_dbo.Registration.Events]
	FOREIGN KEY ([EventID]) REFERENCES [dbo].[Events] ([EventsID]),

	CONSTRAINT [FK_dbo.Registration.CarID]
	FOREIGN KEY ([CarID]) REFERENCES [dbo].[Car] ([CarID]),

	CONSTRAINT [FK_dbo.Registration.ClubID]
	FOREIGN KEY ([ClubsId]) REFERENCES [dbo].[Clubs] ([ClubsID])

);

CREATE TABLE [dbo].[RunSection] (
	[RunSectionCode]			NVARCHAR(8),
	[RunSectionName]			NVARCHAR(64)

	CONSTRAINT [PK_dbo.RunSection] PRIMARY KEY ([RunSectionCode]),
);

CREATE TABLE [dbo].[RunGroup] (
	[RunGroupCode]			NVARCHAR(8),
	[RunGroupName]			NVARCHAR(64)
	
	CONSTRAINT [PK_dbo.RunGroup] PRIMARY KEY ([RunGroupCode]),
);

CREATE TABLE [dbo].[EventResults] (
	[EventResultsID]	INT			IDENTITY (1,1) NOT NULL,
	[RegistrationID]	INT			NOT NULL,
	[RunGroupCode]		NVARCHAR(8)	NOT NULL,
	[RunSectionCode]	NVARCHAR(8)	NULL,
	[CarClassID]		INT			NOT NULL,
	[BestTime]			TIME		NULL,
	[Points]			INT			NULL

	CONSTRAINT [PK_dbo.EventResults] PRIMARY KEY CLUSTERED ([EventResultsID] ASC),

	CONSTRAINT [FK_dbo.EventResults.Registration]
	FOREIGN KEY ([RegistrationID]) REFERENCES [dbo].[Registration] ([RegistrationID]),

	CONSTRAINT [FK_dbo.EventResults.RunGroupCode]
	FOREIGN KEY ([RunGroupCode]) REFERENCES [dbo].[RunGroup] ([RunGroupCode]),

	CONSTRAINT [FK_dbo.EventResults.RunSectionCode]
	FOREIGN KEY ([RunSectionCode]) REFERENCES [dbo].[RunSection] ([RunSectionCode]),

	CONSTRAINT [FK_dbo.EventResults.CarClassID]
	FOREIGN KEY ([CarClassID]) REFERENCES [dbo].[CarClass] ([CarClassID])

);

CREATE TABLE [dbo].[RunData] (
	[RunDataID]			INT		IDENTITY (1,1) NOT NULL,
	[EventResultsID]	INT		NOT NULL,
	[RunNumber]			INT		DEFAULT 1,
	[RawTime]			TIME	NULL,
	[Penalty]			INT		NOT NULL,
	[Finished]			BIT		DEFAULT 1,
	[AdjustedTime]		TIME	NULL

	CONSTRAINT [PK_dbo.RunData] PRIMARY KEY CLUSTERED ([RunDataID] ASC),

	CONSTRAINT [FK_dbo.RunData.EventResults]
	FOREIGN KEY ([EventResultsID]) REFERENCES [dbo].[EventResults] ([EventResultsID])
);