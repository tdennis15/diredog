SET IDENTITY_INSERT Gallery ON
INSERT INTO [dbo].[Gallery] (GalleryID) VALUES
(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21);
SET IDENTITY_INSERT Gallery OFF

INSERT INTO [dbo].[Clubs] (ClubName, ClubIsActive, GalleryID) VALUES
	('Auto X Salem', 1, 1),
	('AutoTest',1, 2)


INSERT INTO [dbo].[MotoHubRoles] (RoleTitle) VALUES
	('ClubAdmin'),
	('ClubMember');


INSERT INTO [dbo].[AspNetUsers] (Id, Email, EmailConfirmed, PasswordHash, SecurityStamp, PhoneNumberConfirmed, TwoFactorEnabled,LockoutEnabled,AccessFailedCount,UserName) Values
('e060c766-3829-4dcf-a7ad-6a7c95e901a6','test@user1.com','False','AIKYuATMr2nfO2cx5rDcj69hoKZFOwMHdcmo54q4UpBLUWo5i2eIN5Ro1YoAGQu0cw==','cebd97f0-d22d-489e-a6ed-900e6b776c38','False','False','True',0,'test@user1.com'),
('5f98efbc-7083-49a6-8634-bd1bc14c31aa','test@user2.com','False','AH3I/8rUpfomI9eEJZBVXfN+g+JeA2Va/DidFFk9KIQKu4lR/K/6/gBjx0L6OznG4g==','7fe4201c-1131-458b-bdc9-1f86efe25d23','False','False','True',0,'test@user2.com'),
('0ef36dc3-3118-4a4f-a69a-55d307838248','test@user3.com','False','AG8om0IdUDinsCHc3DkPKoCUChDvVqHo21v67gwCgiWb8C/kl2LQIF1mM4Ire2B+Yw==','16916fe8-6c35-4448-9aff-5541755d1643','False','False','True',0,'test@user3.com'),
('2a265de5-d8ce-479d-9501-01910d2e8f62','test@user4.com','False','AA7FvIwOs92nY7idYfe1922hOFTM+wqWuqD6xrGlNaJNr65J6OXTeOiGFJE5XEuisA==','726c012b-d372-4676-a6bf-ce908e27cce2','False','False','True',0,'test@user4.com'),
('41a1048b-63bb-4192-82ec-ebe87f3d0a7d','test@user5.com','False','AOSeV2dcEY/LqF3kU5Ll0yMVIWV3dLmJAXpgjm1j71RXpiSJNBaCvyZQ3VCfDnuiJw==','03366085-7774-464c-8644-f8ddf50255ea','False','False','True',0,'test@user5.com'),
('69ff0570-a9dc-4937-a32b-8803383a09e3','test@user6.com','False','AOVlMEQw/wsb/tTdc01KWo3J71yGxVBTI4szqvss+7Zi5zZ9eTZU/bgb1lOk/wcV5A==','93abda21-d0fd-42ce-a4b7-3d913358eebe','False','False','True',0,'test@user6.com'),
('2e237486-7eaa-4e9c-8283-1a2d6ce6e4d8','test@user7.com','False','AFlxSErdmX775ApnaPUlvmUSpsavVdlDcs2JFFcV6YeZlZW73lUQjxBAJ2m/pM1RDA==','af3229ce-7171-4a23-a6f5-6b6ddeb307a8','False','False','True',0,'test@user7.com'),
('8827c9c0-b8fa-4632-8683-9d4552765a61','test@user8.com','False','ACmrymSz6OOAQZx8Xpb04wZoBaHSrYS9TnuwzvEKo8c9/KJOpmb0Jj8xZYEI7LKj5w==','d1dfd76c-5500-4125-8347-9c3700ebb46e','False','False','True',0,'test@user8.com'),
('6178a4bf-f9af-4366-bf71-248d3869a53f','test@user9.com','False','AOx+yItkgmdMyUxvwP+978EoTqGRgkyjnQXgkYBtjy41/+ZsHTryfAAn6ieKdj/Z4w==','cd12db96-1326-45c4-8167-ed42bd931b3f','False','False','True',0,'test@user9.com'),
('0ddaf678-fc69-44b6-9b8b-64891a5c9cc7','test@user10.com','False','AEfI5vXOeHcjDNw6z1ZTB2mkCXL2c/yuZG2yJtoGWb/WURrZTsQwjJ9bZBMwnJx7Ew==','a850d4f5-1675-4eac-a367-c96de95be43b','False','False','True',0,'test@user10.com');

SET IDENTITY_INSERT MotoHubUsers ON
INSERT INTO [dbo].[MotoHubUsers](MotoHubUserID, LoginCount, AspNetIdentityID) Values
(1001,1,'e060c766-3829-4dcf-a7ad-6a7c95e901a6'),
(1002,1,'5f98efbc-7083-49a6-8634-bd1bc14c31aa'),
(1003,1,'0ef36dc3-3118-4a4f-a69a-55d307838248'),
(1004,1,'2a265de5-d8ce-479d-9501-01910d2e8f62'),
(1005,1,'41a1048b-63bb-4192-82ec-ebe87f3d0a7d'),
(1006,1,'69ff0570-a9dc-4937-a32b-8803383a09e3'),
(1007,1,'2e237486-7eaa-4e9c-8283-1a2d6ce6e4d8'),
(1008,1,'8827c9c0-b8fa-4632-8683-9d4552765a61'),
(1009,1,'6178a4bf-f9af-4366-bf71-248d3869a53f'),
(1010,1,'0ddaf678-fc69-44b6-9b8b-64891a5c9cc7');

SET IDENTITY_INSERT MotoHubUsers OFF

INSERT INTO [dbo].[UserProfile] (MotoHubUserID,FirstName,LastName,DateOfBirth,ProfileIsPrivate, GalleryID) Values
(1001, 'Test1', 'Test1','1/1/2001','False', 4),
(1002, 'Test2', 'Test2','1/1/2001','False',5),
(1003, 'Test3', 'Test3','1/1/2001','False',6),
(1004, 'Test4', 'Test4','1/1/2001','False',7),
(1005, 'Test5', 'Test5','1/1/2001','False',8),
(1006, 'Test6', 'Test6','1/1/2001','False',9),
(1007, 'Test7', 'Test7','1/1/2001','False',10),
(1008, 'Test8', 'Test8','1/1/2001','False',11),
(1009, 'Test9', 'Test9','1/1/2001','False',12),
(1010, 'Test10', 'Test10','1/1/2001','False',13);

INSERT INTO [dbo].[Membership] (MotoHubUserID, ClubID, RoleID, Status, DuesPaid) Values
(1001, 2, 1, 'True', 'False'),
(1002, 2, 2, 'True', 'False'),
(1003, 2, 2, 'True', 'False'),
(1004, 2, 2, 'True', 'False'),
(1005, 2, 2, 'True', 'False'),
(1009, 2, 2, 'True', 'False'),
(1010, 2, 2, 'True', 'False'),
(1001, 1, 2, 'True', 'False'),
(1002, 1, 2, 'True', 'False'),
(1007, 2, 2, 'True', 'False');

INSERT INTO [dbo].[Events] (MotoHubUserID, ClubID, EventName, EventDate, EventLocationStreet, EventLocationCity, EventLocationState, EventLocationZip, GalleryID) VALUES
 (1001, 1, 'Ice Breaker', '3/1/2019','1530 Monmouth St', 'Independence', 'OR', 97351, 17),
 (1002, 1, 'Varsity Drinking', '1/1/2019','121 Main St.', 'Monmouth', 'OR', 97361, 18),
 (1001, 2, 'AutoTest Meet & Greet', '1/1/2019','121 Main St', 'Monmouth', 'OR', 97361, 19),
 (1001, 2, 'AutoTest Past Meeting', '04/20/2016', '100 Stony Lane', 'Salem', 'OR', 97350, 20),
 (1001, 2, 'Trial Run Event', '02/10/2016', '1021 Pine Drive', 'Salem', 'OR', 97350, 21);

SET IDENTITY_INSERT PhotoAlbums ON
INSERT INTO [dbo].[PhotoAlbums] (PhotoAlbumID, GalleryID) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20),
(21,21)
SET IDENTITY_INSERT PhotoAlbums OFF

SET IDENTITY_INSERT Garage ON
INSERT INTO [dbo].[Garage] (GarageID, MotoHubUserID) VALUES
(1, 1001),
(2, 1002),
(3, 1003),
(4, 1004),
(5, 1005),
(6, 1006),
(7, 1007),
(8, 1008),
(9, 1009),
(10, 1010)
SET IDENTITY_INSERT Garage OFF


SET IDENTITY_INSERT CarMakes ON
INSERT INTO [dbo].[CarMakes] (CarMakeID, Make) VALUES
(1,'Acura'),(2,'Audi'),(3,'BMW'),(4,'Buick'),(5,'Cadillac'),(6,'Chevrolet'),(7,'Chrysler'),(8,'Dodge'),(9,'Fiat'),(10,'Ford'),(11,'GMC'),
(12,'Honda'),(13,'Hyundai'),(14,'Infiniti'),(15,'Jaguar'),(16,'Jeep'),(17,'Kia'),(18,'Lexus'),(19,'Lotus'),(20,'Mazda'),(21,'Mercedes'),(22,'Mini'),
(23,'Mitsubishi'),(24,'Nissan'),(25,'Porshe'),(26,'Scion'),(27,'Subaru'),(28,'Suzuki'),(29,'Tesla'),(30,'Toyota'),(31,'Volkswagen'),(32,'Volvo'),
(33,'Other')
SET IDENTITY_INSERT CarMakes OFF


SET IDENTITY_INSERT CarColor ON
INSERT INTO [dbo].[CarColor] (CarColorID, Color) VALUES
(1,'White'),(2,'Silver'),(3,'Gray'),(4,'Black'),(5,'Red'),(6,'Maroon'),(7,'Yellow'),(8,'Olive'),(9,'Lime'),(10,'Green'),(11,'Aqua'),
(12,'Teal'),(13,'Blue'),(14,'Navy'),(15,'Fushia'),(16,'Brown'),(17,'Gold'),
(18,'Custom')
SET IDENTITY_INSERT CarColor OFF

SET IDENTITY_INSERT CarType ON
INSERT INTO [dbo].[CarType] (CarTypeID, CarType) VALUES
(1,'Sedan'),(2,'Coupe'),(3,'Hatchback'),(4,'SUV'),(5,'Compact'),(6,'Convertible'),(7,'Pickup'),(8,'Crossover'),(9,'Hybrid'), (10,'OffRoad/Utility'),(11,'Station Wagon'),
(12,'Other')
SET IDENTITY_INSERT CarType OFF


SET IDENTITY_INSERT CarClass ON
INSERT INTO [dbo].[CarClass] (CarClassID, Class) VALUES
(1,'Stock'), (2,'Street'), (3,'Street Mod'), (4,'Touring'), (5,'Gran Touring'), (6,'Pro Mod'), (7,'Rally'),(8,'Production'), (9,'Racing'),
(10,'Other')
SET IDENTITY_INSERT CarClass OFF

SET IDENTITY_INSERT Car ON
INSERT INTO [dbo].[Car] (CarId, MotoHubUserID, GarageID, CarMakeID, CarModel, CarYear, CarColorID, CarTypeID, CarClassID) VALUES
 (1,1001, 1, 30, 'Camry', 2008, 1, 1, 1),
 (2,1001, 1, 24, 'Altima', 2013, 1,1,1),
 (3,1001, 1, 12, 'Accord', 1998, 4, 2, 3),
 (4,1001, 1, 20, 'Miata', 2003, 10, 2, 3)
 SET IDENTITY_INSERT Car OFF

INSERT INTO [dbo].[RunSection] (RunSectionCode, RunSectionName) VALUES
('G1','Group 1'), ('G2', 'Group 2')

INSERT INTO [dbo].[RunGroup] (RunGroupCode, RunGroupName) VALUES
('AM', 'Morning'), ('PM', 'Afternoon')

SET IDENTITY_INSERT Registration ON
INSERT INTO [dbo].[Registration] (RegistrationID, UserID, EventID, CarID, ClubsID) VALUES
(1, 1001, 1, 1, 1),
(2, 1001, 4, 1, 1),
(3, 1001, 5, 1, 1);
SET IDENTITY_INSERT Registration OFF

SET IDENTITY_INSERT EventResults ON
INSERT INTO [dbo].[EventResults] (EventResultsID, RegistrationID, RunGroupCode, RunSectionCode, CarClassID, Points) VALUES
(1, 1,'AM','G2', 1, 8),
(2, 2, 'AM', 'G1', 1, 9),
(3, 3, 'PM', 'G2', 1, 4)
SET IDENTITY_INSERT EventResults OFF

SET IDENTITY_INSERT RunData ON
INSERT INTO [dbo].[RunData] (RunDataID, EventResultsID, RunNumber, Penalty, Finished) VALUES
(1, 1, 1, 0,1),
(2, 1, 2, 1,0),
(3, 1, 3, 0,1),
(4, 2, 1, 0, 1),
(5, 3, 1, 0, 1)
SET IDENTITY_INSERT RunData OFF

GO

CREATE TRIGGER readOnly_tableRunSection ON [dbo].[RunSection]
INSTEAD OF INSERT, UPDATE
AS
BEGIN
	RAISERROR('Table: RunSection is read only', 16, 1)
	ROLLBACK TRANSACTION
END

GO

CREATE TRIGGER readOnly_tableRunGroup ON [dbo].[RunGroup]
INSTEAD OF INSERT, UPDATE
AS
BEGIN
	RAISERROR('Table: RunGroup is read only', 16, 1)
	ROLLBACK TRANSACTION
END

GO


