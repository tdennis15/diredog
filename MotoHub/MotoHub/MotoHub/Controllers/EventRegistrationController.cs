﻿using Microsoft.AspNet.Identity;
using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Models.ViewModels;
using MotoHub.MotoHubDevLibrary;
using MotoHub.Repositories.Persistance;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace MotoHub.Controllers
{
    public class EventRegistrationController : Controller
    {
        private IRegistrationRepository registrationRepository;
        MotoHubDbContext db = new MotoHubDbContext();
        DevTools devTools = new DevTools();
        UnitOfWork unitOfWork = new UnitOfWork(new MotoHubDbContext());

        public EventRegistrationController(IRegistrationRepository registrationRepository)
        {
            this.registrationRepository = registrationRepository;
        }

        [Authorize]
        public ActionResult ManageCreateNew(int? eventID)
        {
            int userID = devTools.GetCurrentUser(db, User.Identity.GetUserId()).MotoHubUserID;
            return RedirectToAction("CreateNew", new { id = eventID , userID });
        }

        [Authorize]
        [HttpGet]
        public ActionResult CreateNew(int? id , int? userID)
        {   
            
            Event @event = db.Events.Find(id);

            if (@event == null)
            {
                return HttpNotFound();
            }

            foreach (var item in db.Registrations)
            {
                int? regID = item.UserID;
                int? regEvent = item.EventID;

                if(@event.EventDate < DateTime.Now)
                {
                    System.Windows.Forms.MessageBox.Show("Event Is Already Over");
                    return RedirectToAction("Details/" + @event.EventsID, "Events");
                }

                if (regID == userID && regEvent == id)
                {
                    System.Windows.Forms.MessageBox.Show("You Are Already Registered For This Event");
                    return RedirectToAction("Details/" + @event.EventsID, "Events");
                }
            }
            RegistrationViewModel viewmodel = CreateRegViewModel(id, userID);

            return View(viewmodel);
        }


        [Authorize]
        [HttpPost]
        public ActionResult CreateNew(System.Web.Mvc.FormCollection form, int userID)
        {
            Registration reg = db.Registrations.Create();

            var FormEvent = form["Event"];
            var FormClub = form["Club"];
            var FormCar = form["Cars"];
            var FormCarClass = form["CarClass"];
            var FormRunGroup = form["RunGroup"];

            reg.UserID = userID;
            reg.EventID = db.Events.Where(m => m.EventName == FormEvent).Select(m => m.EventsID).FirstOrDefault();

            if(FormClub != "")
            {
                reg.ClubsID = db.Clubs.Where(m => m.ClubName == FormClub).Select(m => m.ClubsID).FirstOrDefault();
            }

            if(FormCar != "")
            {
                int carIDFound = Convert.ToInt32(FormCar);
                reg.CarID = db.Cars.Where(c => c.CarID == carIDFound).Select(c => c.CarID).FirstOrDefault();
            }

            if(FormCarClass != "" && FormRunGroup != "")
            {
                int carClassID = unitOfWork.CarClasses.GetCarClassIDFromCarClassName(FormCarClass.ToString());

                if (ModelState.IsValid)
                {
                               
                    db.Registrations.Add(reg);
                    db.SaveChanges();

                    CreateNewEventResultsTableEntry(reg.EventID,reg.RegistrationID, carClassID, FormRunGroup.ToString());
                    return RedirectToAction("Index", "Events");
                }

            }

            return RedirectToAction("CreateNew", new { id = reg.EventID, userID });
        }

        private void CreateNewEventResultsTableEntry(int eventID, int registrationID, int carClassID, string runGroupCode)
        {
            string runSectionCode = GetRunSectionCodeForNewRegistration(eventID);
            
            EventResult newEventResultTable = unitOfWork.EventResults.Create(registrationID, carClassID, runGroupCode, runSectionCode);
            CreateFirstRunDataTableEntry(newEventResultTable.EventResultsID);
        }

        /// <summary>
        /// Determines how many registrations there are in the event and if there is an even number of registrations that have
        /// been created (and thus assined a run section) then they are assigned to Group 1, else they are assigned to Group 2.
        /// </summary>
        /// <param name="eventID">Event that is being registered for.</param>
        /// <returns>String. The runSectionCode.</returns>
        private string GetRunSectionCodeForNewRegistration(int eventID)
        {
            string runSectionCode;

            if(unitOfWork.Events.Get(eventID).Registrations.Count() > 0)
            {
                int numberOfRegistrations = unitOfWork.Events.Get(eventID).Registrations.Count();
                if(numberOfRegistrations % 2 == 0)
                {
                    runSectionCode = "G1";
                }
                else
                {
                    runSectionCode = "G2";
                }
            }
            else
            {
                runSectionCode = "G1";
            }

            return runSectionCode;
        }

        /// <summary>
        /// Creates the first entry in the rundata table for an event attendee.
        /// </summary>
        /// <param name="eventResultsID">EventResults table ID to link to.</param>
        private void CreateFirstRunDataTableEntry(int eventResultsID)
        {
            unitOfWork.RunData.Create(eventResultsID);
        }

        /// <summary>
        /// For creating a new RunData entry with a specified runNumber.
        /// </summary>
        /// <param name="runNumber">Run Number</param>
        /// <param name="eventResults">EventResults table ID to link to.</param>
        private void CreateNewRunDataTableEntry(int eventResults, int runNumber)
        {

        }

        [Authorize]
        public ActionResult RemoveRegistration(int registrationID)
        {
            Registration registrationToUpdate = db.Registrations.Find(registrationID);

            db.Registrations.Remove(registrationToUpdate);
            db.SaveChanges();

            return RedirectToAction("UserHome", "Profile");
        }

        /// <summary>
        /// Test for PBI 266
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        public bool CreatedSuccessfully(Registration registration)
        {
            if (registration.EventID == 0 && registration.UserID == 0)
            {
                throw new Exception("Registration was not created correctly.");
            }
            else
            {
                return true;
            }
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize]
        private RegistrationViewModel CreateRegViewModel(int? eventID, int? userID)
        {

            ViewBag.Event = eventID;
            RegistrationViewModel RVM = new RegistrationViewModel
            {
                Garage = db.Garages.Where(m => m.MotoHubUserID == userID).ToList(),

                Cars = db.Cars.Where(m => m.MotoHubUserID == userID).ToList(),

                Event = db.Events.Where(m => m.EventsID == eventID),

                Club = db.Clubs.Where(m => m.Memberships.Select(a => a.MotoHubUserID).FirstOrDefault() == userID).ToList(),

                Registration = db.Registrations.ToList(),

                UserProfile = db.UserProfiles.Where(m => m.MotoHubUserID == userID),

                CarMake = db.CarMakes.ToList(),

                CarClass = unitOfWork.CarClasses.GetAllCarClasses(),

                RunGroup = unitOfWork.RunGroups.GetAllRunGroups()
                
            };

            return RVM;
        }

        public ActionResult QRCodeScan()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetQRPartial(QRViewModel qr)
        {
            UnitOfWork unitOfWork = new UnitOfWork(db);


            if (qr.Type == "U")
            {
                int uid = Convert.ToInt32(qr.Id);
                MotoHubUser user = unitOfWork.MotoHubUsers.Get(uid);
                return PartialView("_QRUser", user);
            }
            else if (qr.Type == "C")
            {
                int cid = Convert.ToInt32(qr.Id);
                Car car = unitOfWork.Cars.Get(cid);
                return PartialView("_QRCar", car);
            }
            return PartialView("Error");
        }

        private bool CheckValidQRCode(QRViewModel qr)
        {
            bool typeResult = false;
            bool idResult = false;

            if ((qr.Type == "U") || (qr.Type == "C"))
            {
                typeResult = true;
            }
            if (int.TryParse(qr.Id, out int n))
            {
                idResult = true;
            }

            return typeResult && idResult;

        }
        public ActionResult Details()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetQRRegistrationPartial(QRViewModel qr)
        {
            UnitOfWork unitOfWork = new UnitOfWork(db);

            if (qr.Type == "U")
            {
                int uid = Convert.ToInt32(qr.Id);
                IEnumerable<Registration> registrations = unitOfWork.Registrations.GetAllUserRegistrations(uid);
                return PartialView("_UserRegistrations", registrations);
            }
            return PartialView("Error");

        }

    }
}