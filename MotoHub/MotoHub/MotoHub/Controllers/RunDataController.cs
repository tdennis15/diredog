﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MotoHub.DAL;
using MotoHub.Models.ViewModels;
using MotoHub.MotoHubDevLibrary;

namespace MotoHub.Controllers
{
    public class RunDataController : Controller
    {
        DevTools devTools = new DevTools();
        MotoHubDbContext db = new MotoHubDbContext();
        // GET: RunData
        public ActionResult RunData()
        {
            var runData = db.RunDatas.ToList();
            return View(runData);
        }
    }
    
}