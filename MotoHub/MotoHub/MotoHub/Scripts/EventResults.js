﻿$(document).ready(function () {
    $(".animsition").animsition({
        inClass: 'fade-in-left',
        outClass: 'fade-out-right',
        inDuration: 400,
        outDuration: 400,
        linkElement: '.animsition-link',
        // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
        loading: true,
        loadingParentElement: 'body', //animsition wrapper element
        loadingClass: 'animsition-loading',
        loadingInner: '<img src="~/Content/MotoHubGear.svg" />', // e.g '<img src="loading.svg" />'
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: ['animation-duration', '-webkit-animation-duration'],
        // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
        // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
        overlay: false,
        overlayClass: 'animsition-overlay-slide',
        overlayParentElement: 'body',
        transition: function (url) { window.location.href = url; }
    });
});

function filterResults(eventID) {
    var group = $("#run-group-dropdown").val();
    var section = $("#run-section-dropdown").val();
    var test = eventID;

    $.ajax({
        type: "POST",
        url: "/Events/EventResults",
        data: { runGroup: group, runSection: section, eventID: eventID },
        success: function (response) {
            $("#eventResultsTableContainer").html(response);
        },
        error: function () {
            console.log("Some error");
        }
    })
}

