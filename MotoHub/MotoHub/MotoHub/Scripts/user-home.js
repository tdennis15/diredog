﻿let pastEventsChevron = document.getElementById("past-events-chevron");
let upcomingEventsChevron = document.getElementById("upcoming-events-chevron");
let upcomingEventsTable = document.getElementById('upcoming-events-table');
let pastEventsTable = document.getElementById('past-events-table');

upcomingEventsChevron.style.cursor = "pointer";
pastEventsChevron.style.cursor = "pointer";

upcomingEventsChevron.addEventListener('click', showUpcomingEventsTable, false);
pastEventsChevron.addEventListener('click', showPastEventsTable, false);

function showUpcomingEventsTable(e) {
    $("#upcoming-events-table").toggle("show");
};

function showPastEventsTable(e) {
    $("#past-events-table").toggle("show");
};

/*
$("#past-events-chevron").on("click", function () {
    $("#past-events-table").toggle("show");
});
*/