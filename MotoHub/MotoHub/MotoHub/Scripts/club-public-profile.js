﻿$('#members-button').on('change', function (e) {
    e.preventDefault();
    $('#members-list-container').toggle('show');
    $('#events-list-container').toggle(false);
    $('#photos-list-container').toggle(false);
    $('#location-information-container').toggle(false);
})

$('#events-button').on('change', function (e) {
    e.preventDefault();
    $('#members-list-container').toggle(false);
    $('#events-list-container').toggle('show');
    $('#photos-list-container').toggle(false);
    $('#location-information-container').toggle(false);
})

$('#location-button').on('change', function (e) {
    e.preventDefault();
    $('#members-list-container').toggle(false);
    $('#events-list-container').toggle(false);
    $('#photos-list-container').toggle(false);
    $('#location-information-container').toggle('show');
})


$('#photos-button').on('change', function (e) {
    e.preventDefault();
    $('#members-list-container').toggle(false);
    $('#events-list-container').toggle(false);
    $('#location-information-container').toggle(false);
    $('#photos-list-container').toggle("show");
})