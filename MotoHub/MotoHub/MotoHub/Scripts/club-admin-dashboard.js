﻿var allEditWindowButtons = document.getElementsByClassName('edit-window-button');
var allHideEditWindowButtons = document.getElementsByClassName('hide-edit-window-button');

for (var i = 0; i < allEditWindowButtons.length; i++) {
    //Use a 'let' here because it allows for block scope. 
    //Basically when we revisit this area the variables will not be over written by eachother.
    let windowButtonId = allEditWindowButtons[i].attributes.id.nodeValue;
    allEditWindowButtons[i].addEventListener('click', function (e) { toggleEditWindow(e, windowButtonId) }, false);
    allHideEditWindowButtons[i].addEventListener('click', function (e) { toggleEditWindow(e, windowButtonId) }, false);
}

function toggleEditWindow(e, editWindowButtonId) {
    e.preventDefault();
    var fullWindowId = "#" + editWindowButtonId + "-edit-container";
    var buttonId = "#" + editWindowButtonId;

    $(fullWindowId).toggle("show");
    $(buttonId).toggle();
}


//Process a request to the server to display a 'Delete Membership Confirmation' modal.
$(".remove-member-button").on("click", function (e) {
    e.preventDefault();
    e.stopPropagation();

    url = $(this).data('url');

    $.ajax({

        type: 'GET',
        url: url,
        dataType: 'html',
        success: function (response) {
            console.log("success!");

            $('#delete-membership-partial').html(response);
            $('#delete-membership-partial').toggle(true);
            $('#myModal').modal("show");

        },
        error: function () {
            console.log("Some error in ajax.");
        }

    });

});

$("#add-membership-button").on("click", function (e) {
    e.preventDefault();
    e.stopPropagation();

    var id = $(this).data('club');
    $.ajax({

        type: 'GET',
        url: "/Clubs/GetPartialViewAddMembership",
        data: { clubID: id},
        dataType: 'html',
        success: function (response) {
            console.log("success!");
            $('#add-membership-partial').html("");
            $('#add-membership-partial').html(response);
            $('#add-membership-partial').toggle(true);
            $('#add-membership-modal').modal("show");

        },
        error: function () {
            console.log("Some error in ajax.");
        }

    });

})