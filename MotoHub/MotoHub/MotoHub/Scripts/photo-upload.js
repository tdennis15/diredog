﻿let dropBox = document.getElementById('drop-box');
let photoContainer = document.getElementById('photo-container');
let uploadPhotoButton = document.getElementById('upload-photo-button');

//ES5 syntax for adding multiple eventListeners to each item.
;['dragenter', 'dragover'].forEach(eventName => {
    dropBox.addEventListener(eventName, highlight, false);
});

;['dragleave', 'drop'].forEach(eventName => {
    dropBox.addEventListener(eventName, unhighlight, false);
});

dropBox.addEventListener('drop', handleDrop, false);

;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
    dropBox.addEventListener(eventName, preventDefaults, false);
});

uploadPhotoButton.addEventListener('click', toggleUploadWindow, false);

function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
};

function highlight(e) {
    dropBox.classList.add('highlight');
};

function unhighlight(e) {
    dropBox.classList.remove('highlight');
};

/**
 * This will handle the event when a file is dropped into the 'drop box'. It performs the data transfer
 * and also will get out a list of files.
 * param e
 */
function handleDrop(e) {
    let dt = e.dataTransfer
    //fileList Type
    let files = dt.files
    handleFiles(files)
};

/**
 * Converts the FileList type into an array for easy iteration.
 * param files
 */
function handleFiles(files) {
    ([...files]).forEach(uploadFile)
};

function uploadFile(file) {

    if (!checkFileExtension(file)) {
        alert("MotoHub Photo Uploader does not support this file type.")
    }
    else {

        let formData = new FormData();
        var albumID = document.getElementById("form").dataset.albumValue;

        formData.append('albumID', albumID)
        formData.append('file', file)

        $.ajax({
            url: '/Photo/AddPhotoToAlbum/',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                //Reloads the container to show the updated photo
                reloadPhotoContainer();
            }

        });
    };
    //This is an alternative way to send the data.
    /*
    let url = "/Photo/AddPhoto"
    let xhr = new XMLHttpRequest()
    let formData = new FormData()
    xhr.open('POST', url, true)

    xhr.addEventListener('readystatechange', function (e) {
    if (xhr.readyState == 4 && xhr.status == 200) {
        console.log("successfully uploaded file");
    }
    else if (xhr.readyState == 4 && xhr.status != 200) {
        console.log("failed to upload file");
    }
    })

    formData.append('file', file)

    xhr.send(formData);

    */
};

function checkFileExtension(file) {
    let acceptedFileExtensions = ["jpg", "jpeg", "png", "bmp"];

    let fileName = file.name;
    let fileExtension = fileName.split('.')[fileName.split('.').length - 1].toLowerCase();

    let isValid = false;
    for (var i = 0, len = acceptedFileExtensions.length; i < len; i++) {
        if (acceptedFileExtensions[i] == fileExtension) {
            isValid = true;
            return isValid;
        }
        else {
            isValid = false;
        }
    }
    return isValid;
}

function showEditPane(e, photoID) {
    e.preventDefault();

    $("#edit-pane-" + photoID).toggle("show");
};

function reloadPhotoContainer() {
    $('#photo-container').load(location.href + " #photo-container>*", "");
};

function toggleUploadWindow() {
    $('#drop-box-container').toggle("show");
}