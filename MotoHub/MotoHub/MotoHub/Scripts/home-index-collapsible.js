﻿//modified from w3 schools:
//https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_collapsible_animate

var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("mouseover", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    	content.style.maxHeight = content.scrollHeight + "px";
  });
  
  coll[i].addEventListener("mouseout", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    	content.style.maxHeight = null;
  });
}