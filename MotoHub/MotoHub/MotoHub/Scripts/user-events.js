﻿function filterEvents(filterParam) {
    var url;

    if (filterParam == "past") {
        url = "/Profile/GetUserPastEvents";
    }
    else {
        url = "/Profile/GetUserUpcomingEvents";
    }

    $.ajax({
        type: "POST",
        url: url,
        success: function (response) {
            $("#user-events-table").html(response);
        },
        error: function () {
            console.log("Some Error in AJAX.");
        }
    })
}