﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IRunDataRepository : IRepository<RunData>
    {
        RunData GetRunData(int runDataID);
        void Create(int eventResultsID);
        void Create(int eventResultsID, int runNumber);
    }
}
