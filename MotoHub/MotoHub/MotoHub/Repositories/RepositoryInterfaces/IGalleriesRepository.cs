﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IGalleriesRepository : IRepository<Gallery>
    {
        Gallery GetGallery(int galleryID);
    }
}
