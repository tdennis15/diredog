﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MotoHub.Models;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IMotoHubUserRepository : IRepository<MotoHubUser>
    {
        MotoHubUser GetCurrentUser(string id);
        void IncrementLoginCount(int userID);
    }
}