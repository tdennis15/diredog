﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IRoleRepository : IRepository<MotoHubRole>
    {
        MotoHubRole GetRole(string roleTitle);
    }
}
