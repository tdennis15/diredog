﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IRunSectionRepository : IRepository<RunSection>
    {
        RunSection GetRunSection(string runSectionCode);
        IEnumerable<RunSection> GetAllRunSections();
        int Count();
        void SaveChanges();
    }
}
