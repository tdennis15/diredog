﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IMembershipRepository : IRepository<Membership>
    {
        Membership GetMembership(int userID, int clubID);
        Membership GetClubMembership(int clubID, int userID);
    }
}
