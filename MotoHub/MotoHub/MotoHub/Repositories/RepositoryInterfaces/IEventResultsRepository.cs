﻿using MotoHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MotoHub.Repositories.RepositoryInterfaces
{
    public interface IEventResultsRepository : IRepository<EventResult>
    {
        EventResult GetResult(int eventResultID);
        EventResult Create(int registrationID, int carClass, string runGroupCode, string runSectionCode);
        IEnumerable<EventResult> GetAllEventResultsFromEvent(int eventID);
    }
}
