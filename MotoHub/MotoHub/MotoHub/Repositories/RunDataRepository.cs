﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class RunDataRepository : Repository<RunData>, IRunDataRepository
    {
        public RunDataRepository(MotoHubDbContext context) : base(context)
        {

        }
        
        public RunData GetRunData(int runDataID)
        {
            return MotoHubDbContext.RunDatas.Where(rd => rd.RunDataID == runDataID).FirstOrDefault();
        }

        public void Create(int eventResultsID)
        {
            RunData newEntry = new RunData()
            {
                EventResultsID = eventResultsID,
                RunNumber = 1
            };

            MotoHubDbContext.RunDatas.Add(newEntry);
            MotoHubDbContext.SaveChanges();
        }

        public void Create(int eventResultsID, int runNumber)
        {

        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
        
    }
}