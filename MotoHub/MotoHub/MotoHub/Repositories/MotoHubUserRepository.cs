﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MotoHub.Models;
using MotoHub.DAL;
using MotoHub.Repositories.RepositoryInterfaces;
using System.Data.Entity;

namespace MotoHub.Repositories
{
    public class MotoHubUserRepository : Repository<MotoHubUser>, IMotoHubUserRepository
    {
        public MotoHubUserRepository(MotoHubDbContext context) : base(context)
        {
        }

        public MotoHubUser GetCurrentUser(string id)
        {
            return MotoHubDbContext.MotoHubUsers.Where(m => m.AspNetIdentityID.Equals(id)).FirstOrDefault();
        }

        /// <summary>
        /// Increments the login count when a user successfully logs in.
        /// </summary>
        /// <param name="userID">User that logged in.</param>
        public void IncrementLoginCount(int userID)
        {
            MotoHubUser user = MotoHubDbContext.MotoHubUsers.Where(m => m.MotoHubUserID == userID).FirstOrDefault();
            user.LoginCount++;

            MotoHubDbContext.Entry(user).State = EntityState.Modified;
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }

    }
}