﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class CarClassRepository : Repository<CarClass>, ICarClassRepository
    {
        public CarClassRepository(MotoHubDbContext context) : base(context)
        {

        }

        public CarClass GetCarClass(int carClassID)
        {
            return MotoHubDbContext.CarClasses.Where(cc => cc.CarClassID == carClassID).FirstOrDefault();
        }

        public IEnumerable<CarClass> GetAllCarClasses()
        {
            return MotoHubDbContext.CarClasses.ToList();
        }

        public int GetCarClassIDFromCarClassName(string carClassName)
        {
            return MotoHubDbContext.CarClasses.Where(cc => cc.Class.Equals(carClassName))
                                              .Select(cc => cc.CarClassID)
                                              .FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}