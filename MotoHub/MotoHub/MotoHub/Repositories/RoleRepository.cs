﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace MotoHub.Repositories
{
    public class RoleRepository : Repository<MotoHubRole>, IRoleRepository
    {
        public RoleRepository(MotoHubDbContext context) : base(context)
        {

        }

        public MotoHubRole GetRole(string roleTitle)
        {
            return MotoHubDbContext.MotoHubRoles.Where(mr => mr.RoleTitle.Equals(roleTitle))
                                                .FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}