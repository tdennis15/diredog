﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class EventResultsRepository : Repository<EventResult>, IEventResultsRepository
    {
        public EventResultsRepository(MotoHubDbContext context) : base(context)
        {

        }

        public EventResult GetResult(int eventResultID)
        {
            return MotoHubDbContext.EventResults.Where(er => er.EventResultsID == eventResultID).FirstOrDefault();
        }

        public EventResult Create(int registrationID, int carClass, string runGroupCode, string runSectionCode)
        {
            EventResult newEventResult = new EventResult()
            {
                RegistrationID = registrationID,
                CarClassID = carClass,
                RunGroupCode = runGroupCode,
                RunSectionCode = runSectionCode,
                Points = 0,

            };

            MotoHubDbContext.EventResults.Add(newEventResult);
            MotoHubDbContext.SaveChanges();

            return newEventResult;
        }
        
        public IEnumerable<EventResult> GetAllEventResultsFromEvent(int eventID)
        {
            return MotoHubDbContext.EventResults.Where(er => er.Registration.EventID == eventID).ToList();
        }
        
        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}