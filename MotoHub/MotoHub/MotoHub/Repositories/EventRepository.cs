﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
    public class EventRepository : Repository<Event>, IEventRepository
    {
        public EventRepository(MotoHubDbContext context) : base(context)
        {
        }
        
        public Event GetEvent(int eventID)
        {
            return MotoHubDbContext.Events.Where(e => e.EventsID == eventID).FirstOrDefault();
        }

        public void Insert(Event @event)
        {
            MotoHubDbContext.Events.Add(@event);
        }

        public IEnumerable<Event> GetAllEventsFromListOfRegistrations(IEnumerable<Registration> registrations)
        {
            List<Event> events= new List<Event>();
            foreach(var registration in registrations)
            {
                events.Add(MotoHubDbContext.Events.Where(e => e.EventsID == registration.EventID).FirstOrDefault());
            }
            return events;
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
    
}