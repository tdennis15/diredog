﻿using MotoHub.DAL;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Repositories
{
   public class GarageRepository : Repository<Garage>, IGarageRepository
    {
        public GarageRepository(MotoHubDbContext context) : base(context)
        {

        }

        public Garage GetGarage(int garageID)
        {
            return MotoHubDbContext.Garages.Where(g => g.GarageID == garageID)
                                                .FirstOrDefault();
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }
    }
}