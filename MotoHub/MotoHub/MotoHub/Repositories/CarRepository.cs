﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MotoHub.Models;
using MotoHub.Repositories.RepositoryInterfaces;
using MotoHub.DAL;

namespace MotoHub.Repositories
{
    public class CarRepository : Repository<Car>, ICarRepository
    {
        public CarRepository(MotoHubDbContext context) : base(context)
        {
        }

        public MotoHubDbContext MotoHubDbContext
        {
            get { return Context as MotoHubDbContext; }
        }

    }
}