namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RunGroup")]
    public partial class RunGroup
    {
        [Key]
        [StringLength(8)]
        public string RunGroupCode { get; set; }

        [StringLength(64)]
        public string RunGroupName { get; set; }
    }
}
