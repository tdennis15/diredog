namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Club
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Club()
        {
            Events = new HashSet<Event>();
            Memberships = new HashSet<Membership>();
            Registrations = new HashSet<Registration>();
        }

        [Key]
        public int ClubsID { get; set; }

        [Required]
        [StringLength(50)]
        public string ClubName { get; set; }

        public int? ClubSize { get; set; }

        public bool ClubIsActive { get; set; }

        public string ClubDescription { get; set; }

        [StringLength(64)]
        public string ClubLocationStreet { get; set; }

        [StringLength(64)]
        public string ClubLocationCity { get; set; }

        [StringLength(64)]
        public string ClubLocationState { get; set; }

        public int? ClubLocationZip { get; set; }

        public int? ProfilePhotoID { get; set; }

        public int? GalleryID { get; set; }

        public virtual Gallery Gallery { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Membership> Memberships { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Registration> Registrations { get; set; }

        public virtual Photo Photo { get; set; }
    }
}
