﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class EventAdminEditViewModel
    {
        public bool ShowAdminControl { get; set; }
        public Event Event { get; set; }
    }
}