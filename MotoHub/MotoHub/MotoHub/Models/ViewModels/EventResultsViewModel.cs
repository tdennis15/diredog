﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class EventResultsViewModel
    {
        public IEnumerable<EventResult> EventResults { get; set; }
        public IEnumerable<RunData> RunData { get; set; }
        public IEnumerable<RunGroup> RunGroup { get; set; }
        public IEnumerable<RunSection> RunSection { get; set; }
    }
}