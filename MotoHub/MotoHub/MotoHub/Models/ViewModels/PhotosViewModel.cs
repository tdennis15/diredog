﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class PhotosViewModel
    {
        public IEnumerable<Club> Clubs { get; set; }
        public IEnumerable<UserProfile> UserProfiles { get; set; }
        public IEnumerable<Event> Events { get; set; }
        public IEnumerable<Photo> Photos { get; set; }
    }
}