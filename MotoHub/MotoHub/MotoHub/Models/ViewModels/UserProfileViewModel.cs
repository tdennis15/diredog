﻿using MotoHub.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotoHub.Models.ViewModels
{
    public class UserProfileViewModel
    {
        public IEnumerable<MotoHubUser> MotoHubUsers { get; set; }
        public IEnumerable<Membership> Memberships { get; set; }
        public IEnumerable<Club> Clubs { get; set; }
        public IEnumerable<UserProfile> UserProfile { get; set; }
        public IEnumerable<Garage> Garage { get; set; }
        public IEnumerable<Car> Car { get; set; }
        public IEnumerable<CarClass> CarClass { get; set; }
        public IEnumerable<CarColor> CarColor { get; set; }
        public IEnumerable<CarMake> CarMake { get; set; }
        public IEnumerable<CarType> Cartype { get; set; }
        public IEnumerable<Photo> Photos { get; set; }
        public IEnumerable<Registration> Registration { get; set; }
        public IEnumerable<Event> Event { get; set; }
        //Should eventually hold things like clubs and other user related items from the db.
    }
}