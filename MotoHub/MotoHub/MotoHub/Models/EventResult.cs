namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EventResult
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EventResult()
        {
            RunDatas = new HashSet<RunData>();
        }

        [Key]
        public int EventResultsID { get; set; }

        public int RegistrationID { get; set; }

        public string RunGroupCode { get; set; }

        public string RunSectionCode { get; set; }

        [Required]
        public int CarClassID { get; set; }

        public TimeSpan? BestTime { get; set; }

        public int? Points { get; set; }

        public virtual Registration Registration { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RunData> RunDatas { get; set; }
    }
}
