namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RunData")]
    public partial class RunData
    {
        public int RunDataID { get; set; }

        public int EventResultsID { get; set; }

        public int? RunNumber { get; set; }

        [Column(TypeName = "time")]
        [DisplayFormat(DataFormatString ="{0:hh\\:mm\\:ss\\.ff}", ApplyFormatInEditMode = true)]
        public TimeSpan? RawTime { get; set; }

        public int Penalty { get; set; }

        public bool? Finished { get; set; }

        [Column(TypeName = "time")]
        [DisplayFormat(DataFormatString ="{0:hh:mm:ss.ff}", ApplyFormatInEditMode = true)]
        public TimeSpan? AdjustedTime { get; set; }

        public virtual EventResult EventResult { get; set; }
    }
}
