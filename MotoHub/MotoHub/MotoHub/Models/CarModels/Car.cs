namespace MotoHub.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Car")]
    public partial class Car
    {
       //[DatabaseGenerated(DatabaseGeneratedOption.None)]

        [Key]
        public int CarID { get; set; }

        public int MotoHubUserID { get; set; }

        public int? GuestID { get; set; }

        public int GarageID { get; set; }

        public int CarMakeID { get; set; }

        [Required]
        [StringLength(64)]
        public string CarModel { get; set; }

        public int CarYear { get; set; }

        public int? CarColorID { get; set; }

        public int? CarTypeID { get; set; }

        public int? CarClassID { get; set; }

        [StringLength(128)]
        public string CarQRCode { get; set; }

        public virtual CarClass CarClass { get; set; }

        public virtual CarType CarType { get; set; }

        public virtual CarColor CarColor { get; set; }

        public virtual Garage Garage { get; set; }

        public virtual CarMake CarMake { get; set; }
    }
}