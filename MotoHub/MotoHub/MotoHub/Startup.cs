﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MotoHub.Startup))]
namespace MotoHub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
