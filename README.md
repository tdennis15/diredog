# *Welcome to the Homepage of DireDog*

*Note: We have added QRCoder NuGet package to the project. Please see the 'Tools ans Versions' section at the bottom of the page.*

This is the central location for all of DireDog's work items for the senior sequence classes CS 461 and 462.
On this site you will find the following documents and code:

* The class project 'Fitness Tracker'.
* Milestones 1 through 5.
  * Milestone #1 contains:
    * The [resumes](https://bitbucket.org/JTMozingo/diredog/src/771a5c7534c3b3bdbd346ddb59a1f8c140163f61/Milestone%231/Resumes/?at=development) of each project member.
    * The initial concept ideas for three possible project ideas.
    * DireDog letterheads/business cards/logo/scheduled meeting times.
    * [A contributors page.](https://bitbucket.org/JTMozingo/diredog/src/771a5c7534c3b3bdbd346ddb59a1f8c140163f61/Milestone%231/Contributors.txt?at=development&fileviewer=file-view-default)
  * Milestone #2 contains:
    * User stories, needs, features and requirements for the class project.
    * Architecture, ER, Flow and Use Case Diagrams.
    * The initial vision statement.
    * And a project QA exercise to develop a larger vision of potential project ideas.
  * Milestone #3 contains:
    * The initial vision statement for [MotoHub.](https://bitbucket.org/JTMozingo/diredog/src/771a5c7534c3b3bdbd346ddb59a1f8c140163f61/Milestone%233/TeamProject-Vision0-1.md?at=development&fileviewer=file-view-default)
    * Team project [MindMap](https://bitbucket.org/JTMozingo/diredog/src/771a5c7534c3b3bdbd346ddb59a1f8c140163f61/Milestone%233/MS3TeamProject-MindMap.jpg?at=development&fileviewer=file-view-default) and ER diagrams.
    * User Stories for MotoHub.
  * Milestone #4 contains:
    * Sprint development/release planning sheet.
    * Developed project diagrams, requirements, needs and features and [vision statement.](https://bitbucket.org/JTMozingo/diredog/src/771a5c7534c3b3bdbd346ddb59a1f8c140163f61/Milestone%234/TeamProject-VisionStatement.md?at=development&fileviewer=file-view-default)
* The group project that we will be working on the rest of the year, [MotoHub.](https://bitbucket.org/JTMozingo/diredog/src/771a5c7534c3b3bdbd346ddb59a1f8c140163f61/MotoHub/MotoHub/MotoHub/?at=development)

# *Motohub*

---

This project was created with the hope of increasing the productivity of AutoCross clubs around America
and bring together people of similar interests! This club management application will allow our users to
manage their clubs events and their club members and allow club members a chance to interact with each other,
join clubs and events and track their competition results.

View our live site at: http://motohub.azurewebsites.net
Listen to our team song. https://www.youtube.com/watch?v=9so3U_27tVo

---

*Vision Statement*

Motohub is the apex of the integration of social circles and event management and planning.
MotoHub is for car enthusiasts who want to participate in sports car club events both locally
and regionally. Motor sports club organizers can manage events, members and committees easily. 
Through MotoHub, club owners and event hosts can organize and distribute their registration 
documents and online forms, maintain their schedules, create budget plans and view up-to-date 
analytics and stats on their events and club members. MotoHub provides a user centric space for 
Club Members to interact with each other, and members of other clubs. They can sign-up for events 
and apply for club membership, view detailed information on clubs and events. MotoHub offers 
a space for Club Members to show off their vehicles and connect with other enthusiasts within 
the MotoHub network, by allowing users to list details on their vehicles and interests. MotoHub
 aims to be the one-stop-shop for all motor sports club organization and connection, to provide 
consistency and foster community.

#### Thanks from the project creators,
##### Alex LeClerc, TJ Dennis and JT Mozingo

---

### *Want to help out? Here are some tips.*

*Coding Standards*

* We will be using the C# Standard as defined here: [Microsoft C# Conventions.](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/coding-conventions)
* Use XML Style comments at the top of all public methods.
* Please make sure that all your variable names are meaningful! We want to actually know what
  we are reading! If you find trouble with this please consult the second chapter of [Clean Code.](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)
* If you are creating a database table we will be using 'tableNamePK'. So if you table is Athetes, please use: AthletesPK.
*Git Procedure*

* Please fork the repo here: [DireDog Development Repo](https://bitbucket.org/JTMozingo/diredog/src/771a5c7534c3b3bdbd346ddb59a1f8c140163f61/?at=development)
* We will be using the branches: master -> release -> development -> feature. When you fork the initial repo,
  please pull from development to get the latest build. Any pull requests that you make should be proceeded by
  pulling down the upstream development branch to make sure you have the most updated code and resolving any merge
  conflicts before creating the actual PR.
* All PR's should be made to the development branch where the code can be tested for bugs and small last minute additions
  can be added to the code base before being merged into the live branch.
* All commit messages should be clear as to what has been done. Please remember to commit early and often.
* Please report any bugs to the [issues](https://bitbucket.org/JTMozingo/diredog/issues?status=new&status=open) section of the repository.
 
#### Check Us Out Here at our Visual Studios Team Services [page.](https://diredog.visualstudio.com/MotoHub)


*Tools and Versions That We Use*

*  Visual Studio Community 2017 (v. 15.5.6)
*  Bitbucket.
*  Azure Web Services.
*  NHSTA API.
*  Bootstrap (v. 3.0.0)
*  jQuery (v. 1.10.2)
*  ASP.NET MVC (v. 5.2.3)
*  ASP.NET Razor (v. 3.2.3)
*  Newtonsoft.JSON (v. 6.0.4)
*  Modernizer (v. 2.6.2)
*  .NET Compilers (v. 2.1.0)
*  NuGet QRCoder Package (v. 1.3.2)
*  Agile Development Methodology.
*  If any of these are updated there will be a post at the top of this page and
   the list here will be updated.


