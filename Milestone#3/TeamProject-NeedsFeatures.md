Needs / Features

* Uses NHTSA API
* User registration
    * Each user has a QR code for easy registration
        * This can be displayed on a badge or membership card
    * User can register cars in a 'garage'
        * Pull information from NHTSA API to determine parts on users car and make notes on modifications
    * Register for events

* Club Committee management tools
    * Certain users are part of club committees
    * Host events with CRUD priveleges
    * Upload event and club materials 
        * Central area to hold forms for all club admins (DropBox API, box, etc)
    * Mass emailing for event updates/news
    * Admin dashboard/analysis of events
* Event information pages such as scheduling, event materials, maps, etc

    

