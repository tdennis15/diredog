# DireDog
## Milestone 2 Class Project
### User Stories
1. As an Athlete I want to be able to edit my user information so in case something changes in my contact information or my emergency information or even my name I can easily update it.  
    - 2 
2. As an Athlete I want to be able to access my records of individual runs to be able to see an overview of my stats like heartrate, cadence, time, distance and locations to better keep track of my training progress.
    - 
4. As a coach I want to be able to track my teams progress and an individual’s progress over the years they participate in my sport to see if new coaching methods are working.
5. As a coach I want to be able to update my contact information on my coach page to allow my athletes to be able to get ahold of me in case they can’t make it to practice or in case of an emergency. 
    - 2
6. As a coach I want to be able to receive some sort of notification if a member of my team hasn’t completed their training for the day to find out why they didn’t train.
7. As an Athlete I want to be notified if I have broken a personal record so I can feel really, really good about myself and show off to all my friends and maybe my daddy will love me again.
    - Combine with 10 as an Epic
8. As an Athlete I want to be able to see the analysis of my training so I can see where I need to improve.
    - Epic
9. As a coach, I want the ability to add and drop members off of my team to reflect the actual make up of my team.
    - Mid - flag in database
10. As a coach I want to see if an athlete has broken a personal record to congratulate them on their achievements, and see their training analysis to determine any trends to their success.
    - Combine with 7 as an Epic
11. As a coach I want to be able to see my teams progress as a group overall in order to gauge team performance.
    - 
12. As a coach I want to be able to see top and lowest performers in my team so I can focus on those individuals.
    - Mid feature: Add decathelon points
13. As a coach I want to see the athletes who have shown the most or least improvements so I can focus my attention on those individuals.
    - Mid feature: Add decathelon points
    
14. As a coach I want to be able to see decathlon points of my athletes in order to 