namespace FitnessTracker.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RunData")]
    public partial class RunData
    {
        [Key]
        public int RunID { get; set; }

        public int AthleteID { get; set; }

        public decimal? Distance { get; set; }

        public decimal? RunTime { get; set; }

        public decimal? HeartRate { get; set; }

        public decimal? StepsPMin { get; set; }

        public virtual Athlete Athlete { get; set; }
    }
}
