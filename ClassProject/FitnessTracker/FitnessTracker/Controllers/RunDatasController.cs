﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessTracker.Models;

namespace FitnessTracker.Controllers
{
    public class RunDatasController : Controller
    {
        private FitnessContext db = new FitnessContext();

        // GET: RunDatas
        public ActionResult Index()
        {
            var runDatas = db.RunDatas.Include(r => r.Athlete);
            return View(runDatas.ToList());
        }

        // GET: RunDatas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RunData runData = db.RunDatas.Find(id);
            if (runData == null)
            {
                return HttpNotFound();
            }
            return View(runData);
        }

        // GET: RunDatas/Create
        public ActionResult Create()
        {
            ViewBag.AthleteID = new SelectList(db.Athletes, "ID", "FirstName");
            return View();
        }

        // POST: RunDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RunID,AthleteID,Distance,RunTime,HeartRate,StepsPMin")] RunData runData)
        {
            if (ModelState.IsValid)
            {
                db.RunDatas.Add(runData);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AthleteID = new SelectList(db.Athletes, "ID", "FirstName", runData.AthleteID);
            return View(runData);
        }

        // GET: RunDatas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RunData runData = db.RunDatas.Find(id);
            if (runData == null)
            {
                return HttpNotFound();
            }
            ViewBag.AthleteID = new SelectList(db.Athletes, "ID", "FirstName", runData.AthleteID);
            return View(runData);
        }

        // POST: RunDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RunID,AthleteID,Distance,RunTime,HeartRate,StepsPMin")] RunData runData)
        {
            if (ModelState.IsValid)
            {
                db.Entry(runData).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AthleteID = new SelectList(db.Athletes, "ID", "FirstName", runData.AthleteID);
            return View(runData);
        }

        // GET: RunDatas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RunData runData = db.RunDatas.Find(id);
            if (runData == null)
            {
                return HttpNotFound();
            }
            return View(runData);
        }

        // POST: RunDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RunData runData = db.RunDatas.Find(id);
            db.RunDatas.Remove(runData);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
