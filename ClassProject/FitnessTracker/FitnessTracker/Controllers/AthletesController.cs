﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessTracker.Models;
namespace FitnessTracker.Controllers
{
    public class AthletesController : Controller
    {
        FitnessContext db = new FitnessContext();
       

        [HttpGet]
        public ActionResult AthleteList()
        {
            var athlete = db.Athletes.ToList();
            return View(athlete);
        }



        [HttpGet]
        public ActionResult AthleteCreate()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AthleteCreate(FormCollection NewAth)
        {
            try
            {
                    Athlete newAthlete = db.Athletes.Create();

                    newAthlete.FirstName = NewAth["FirstName"];
                    newAthlete.LastName = NewAth["LastName"];

                    newAthlete.Age = NewAth["Age"];
                    newAthlete.Squad = Int32.Parse(NewAth["Squad"]);

                    db.Athletes.Add(newAthlete);

                    db.SaveChanges();
                    
                    return RedirectToAction("AthleteList");
                
            }
            catch
            {
                return RedirectToAction("AthleteCreate");
            }
        }



        [HttpGet]
        public ActionResult AthleteDelete(int id)
        {
            var athlete = db.Athletes.Where(a => a.ID == id).FirstOrDefault();
           return View(athlete);
        }

        [HttpPost]
        public ActionResult AthleteDelete(int id, FormCollection collection)
        {
            try
            {
                var athlete = db.Athletes.Find(id);

                db.Athletes.Remove(athlete);
                db.SaveChanges();

                return RedirectToAction("AthleteList");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult AthleteDetails(int id)
        {
            var athlete = db.Athletes.Where(a => a.ID == id).FirstOrDefault();
            return View(athlete);
        }


        [HttpGet]
        public ActionResult AthleteUpdate(int id)
        {
            var athlete = db.Athletes.Where(a => a.ID == id).FirstOrDefault();
            return View(athlete);
        }


        [HttpPost]
        public ActionResult AthleteUpdate(int id, FormCollection collection)
        {
            try
            {
                var AthleteToUpdate = db.Athletes.Find(id);

                AthleteToUpdate.FirstName = collection["FirstName"];
                AthleteToUpdate.LastName = collection["LastName"];
                AthleteToUpdate.Age = collection["Age"];
                AthleteToUpdate.Squad = Int32.Parse(collection["Squad"]);


                db.SaveChanges();

                return RedirectToAction("AthleteDetails/" + id);
            }
            catch
            {
                return RedirectToAction("AthleteList");
            }
        }
    }
}