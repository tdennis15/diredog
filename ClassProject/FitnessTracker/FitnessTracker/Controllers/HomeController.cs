﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FitnessTracker.Models;

namespace FitnessTracker.Controllers
{
    public class HomeController : Controller
    {

        private FitnessContext db = new FitnessContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Athlete()
        {
            var athletes = db.Athletes;
            return View(athletes);
        }

    }
}