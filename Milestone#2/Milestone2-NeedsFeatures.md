# DireDog 
## Class Project
### Needs and Features

* Abstract trends on dashboard page (before login)
    * Login required to view discrete data points and individuals performance.
* Data representation
	* Functionality to look at individual team members performance and stats
        * Athlete can only view their own individual data
	* Can look at some users who are improving immensely or not improving
	* Different tabs to transition from main dashboard to list of atheletes
    * A page that shows overall team performance
    * Overall **Team performance** viewable by coach and athletes
* Coach has view privileges
    * If you are a coach with admin privledges you can update/change/delete


* Utilize **GPS watch**, **foot pod**, and **heart rate monitor** chest strap