# DireDog
## Milestone 2 : Class Project
### Requirements

* Garmin code to extract data
* Database : relational tables and roles
* Web interface: to monitor data 
* Encapsulated .fit file
* Analysis on web interface
* Students and coaches will need some way to log into the interface. (Through their IDs)
    * Authentication
* Certain Coaches will have admin privledges

